<?php

namespace ApiBundle\DependencyInjection\Security;

use Symfony\Component\HttpFoundation\Response;
use OAuth2\OAuth2ServerException;
use Symfony\Component\HttpFoundation\Request;
use OAuth2\OAuth2AuthenticateException;
use Doctrine\ORM\EntityManager;
use OAuth2\IOAuth2Storage;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use FOS\UserBundle\Model\UserInterface;
use AppBundle\Entity\User;
use OAuth2\IOAuth2GrantUser;
use ApiBundle\DependencyInjection\Storage\OAuthStorage;
use ApiBundle\DependencyInjection\Storage\ApiStorage;
use ApiBundle\Model\Auth;

/**
 * ApiAuthenticator class.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiAuthenticator extends Auth
{
	/**
	 * The access token provided is invalid
	 * 
	 * */
	const ERROR_INVALID_TOKEN = 'invalid_token';
	
	/**
	 * The access token provided has expired
	 *
	 * */
	const ERROR_EXPIRED_TOKEN = 'expired_token';
	
	protected $entityManager;
	
	protected $storage;
	
	/**
	 * {@inheritdoc}
	 */
	public function __construct(EntityManager $entityManager, ApiStorage $storage)
	{
		parent::__construct($storage, array());
		
		$this->entityManager = $entityManager;
		$this->storage = $storage;
	}
	
    /**
     * {@inheritdoc}
     */
    public function grantAccessToken(Request $request = null){
    	$filters = array(
    			"grant_type" => array(
   					"filter" => FILTER_VALIDATE_REGEXP,
   					"options" => array("regexp" => self::GRANT_TYPE_REGEXP),
   					"flags" => FILTER_REQUIRE_SCALAR
    			),
    			"email" => array("flags" => FILTER_REQUIRE_SCALAR),
    			"password" => array("flags" => FILTER_REQUIRE_SCALAR),
    	);
    	
    	if ($request === null) {
    		$request = Request::createFromGlobals();
    	}
    	
    	// Input data by default can be either POST or GET
   		$inputData = $request->request->all();
    	
    	// Basic authorization header
    	$authHeaders = $this->getAuthorizationHeader($request);
    	
    	// Filter input data
    	$input = filter_var_array($inputData, $filters);

    	// Grant Type must be specified.
    	if (!$input["grant_type"]) {
    		throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Invalid grant_type parameter or parameter missing');
    	}
    	
//TODO Pending to create custom AuthenticationInterface and UserProvider to handle with
    	// Authorize the user
    	$email = $input['email'];
    	$password = $input['password'];
		$user = $this->entityManager->getRepository('AppBundle:User')->findOneBy(array('email' => $email));

    	if (!$user) {
    		throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'The client credentials are invalid');
    	}

    	// Do the granting
    	switch ($input["grant_type"]) {
    		case self::GRANT_TYPE_USER_CREDENTIALS:
    			$stored = $this->storage->checkUserCredentials($user, $password);
    			break;
    		default:
   				throw new OAuth2ServerException(
  						self::HTTP_BAD_REQUEST,
   						self::ERROR_INVALID_REQUEST,
    						'Invalid grant_type parameter or parameter missing'
   				);
    	}
    	
    	if (!is_array($stored)) {
    		$stored = array();
    	}
    	
    	// if no scope provided to check against $input['scope'] then application defaults are set
    	// if no data is provided than null is set
    	$stored += array('scope' => $this->getVariable(self::CONFIG_SUPPORTED_SCOPES, null), 'data' => null,
    			'access_token_lifetime' => $this->getVariable(self::CONFIG_ACCESS_LIFETIME),
    			'issue_refresh_token' => true, 'refresh_token_lifetime' => $this->getVariable(self::CONFIG_REFRESH_LIFETIME));
    	
    	$scope = $stored['scope'];
    	
    	$token = $this->createAccessToken($stored['data'], $scope, $stored['access_token_lifetime'], $stored['issue_refresh_token'], $stored['refresh_token_lifetime']);
    	return new Response(json_encode($token), 200);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createAccessToken($user, $scope = null, $access_token_lifetime = null, $issue_refresh_token = true, $refresh_token_lifetime = null)
    {
    	$token = array(
    			"access_token" => $this->genAccessToken(),
    			"expires_in" => ($access_token_lifetime ?: $this->getVariable(self::CONFIG_ACCESS_LIFETIME)),
    			"token_type" => $this->getVariable(self::CONFIG_TOKEN_TYPE),
    			"scope" => $scope,
    	);
    
    	$this->storage->createAccessToken(
    			$token["access_token"],
    			$user,
    			time() + ($access_token_lifetime ?: $this->getVariable(self::CONFIG_ACCESS_LIFETIME)),
    			$scope
    	);
    
    	return $token;
    }

}
