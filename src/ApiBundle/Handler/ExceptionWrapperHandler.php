<?php
namespace ApiBundle\Handler;

use FOS\RestBundle\Util\ExceptionWrapper;
use FOS\RestBundle\View\ExceptionWrapperHandlerInterface;

/**
 * ExceptionWrapperHandler class.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ExceptionWrapperHandler implements ExceptionWrapperHandlerInterface {

    public function wrap($data)
    {
        /** @var \Symfony\Component\Debug\Exception\FlattenException $exception */
        $exception = $data['exception'];

        $newException = array(
            'status' => 'KO',
            'errorCode' => 'ERR'.$exception->getStatusCode(),
            'error_description' => $data['status_text']// . '. '.$exception->getMessage()
        );

        return $newException;
    }
}