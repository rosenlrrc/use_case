<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use AppBundle\Factory\ProductFactory;
use ApiBundle\Exception\ResourceNotFoundException;
use ApiBundle\Exception\AccessDeniedException;

/**
 * ProductController.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ProductController extends FOSRestController
{

	/**
	 * Displays a list of Product entities.
	 *
	 */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$productFactory = new ProductFactory($em);
    	$products = $productFactory->getProductsList();

    	return $this->apiResponse($products);
    }
    
    /**
     * Displays a Product entity.
     *
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$productFactory = new ProductFactory($em);
    	$product = $productFactory->getProductDetail($id);
    	
    	return $this->apiResponse($product);
    }
    
    /**
     * Creates a new Product entity.
     *
     */
    public function createAction()
    {
    	if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
    		throw new AccessDeniedException();
    	}
    	
    	$request = $this->getRequest();
		$data = $request->request->all();

    	$product = new Product();
    	$form = $this->createForm(new ProductType(), $product);
    	$form->submit($data);
    	    	
		$em = $this->getDoctrine()->getManager();
		$em->persist($product);
		$em->flush();
    	    	
    	return $this->apiResponse();
    }
    
    /**
     * Updates an existing Product entity.
     *
     */
    public function updateAction($id)
    {
    	if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
    		throw new AccessDeniedException();
    	}
    	
    	$request = $this->getRequest();
    	$data = $request->request->all();

    	$em = $this->getDoctrine()->getManager();
    	
    	$product = $em->getRepository('AppBundle:Product')->find($id);
    	
    	if(!$product instanceof Product){
    		throw new ResourceNotFoundException();	
    	}
    	
    	$form = $this->createForm(new ProductType(), $product);
    	$form->submit($data);
    	
    	$em->persist($product);
    	$em->flush();
    	 
    	return $this->apiResponse();
    }
    
    /**
     * Deletes an existing Product entity.
     *
     */
    public function deleteAction($id)
    {
    	if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
    		throw new AccessDeniedException();
    	}
    	
    	$em = $this->getDoctrine()->getManager();
    	
    	$product = $em->getRepository('AppBundle:Product')->find($id);

    	if(!$product instanceof Product){
    		throw new ResourceNotFoundException();
    	}
    	
    	$form = $this->createDeleteForm($product);
    	
    	$em->remove($product);
    	$em->flush();
    	
    	return $this->apiResponse();
    }
    
    /**
     * Creates a form to delete a Product entity.
     *
     * @param Product $product The Product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('api_product_delete', array('id' => $product->getId())))
    	->setMethod('DELETE')
    	->getForm()
    	;
    }
    
    /**
     * Normalize response structure.
     * 
     * @param array $responseData
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function apiResponse($responseData = array()){
    	$response = array(
    		'status' => 'OK',
    		'data' => $responseData
    	);
    	
    	return new JsonResponse($response);
    }
}
