<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use OAuth2\OAuth2ServerException;
use FOS\RestBundle\Controller\FOSRestController;
use ApiBundle\DependencyInjection\Security\ApiAuthenticator;

/**
 * AuthenticateController.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class AuthenticateController extends FOSRestController
{

	/**
	 * @var ApiAuthenticator
	 */
	protected $server;
	
	/**
	 * @param ApiAuthenticator $server
	 */
	public function __construct(ApiAuthenticator $server)
	{
		$this->server = $server;
	}
	
	/**
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function loginAction(Request $request)
	{
		try {
			return $this->server->grantAccessToken($request);
		} catch (OAuth2ServerException $e) {
			return $e->getHttpResponse();
		}
	}
	
}
