<?php

namespace ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use ApiBundle\DependencyInjection\Factory\AuthenticatorFactory;
use Symfony\Component\HttpKernel\Kernel;

class ApiBundle extends Bundle
{
	public function build(ContainerBuilder $container)
	{
		parent::build($container);
	
		if (version_compare(Kernel::VERSION, '2.1', '>=')) {
			$extension = $container->getExtension('security');
			$extension->addSecurityListenerFactory(new AuthenticatorFactory());
		}

	}
}
