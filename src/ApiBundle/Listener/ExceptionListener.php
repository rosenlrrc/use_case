<?php

namespace ApiBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use ApiBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\Symfony\Component\HttpFoundation;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * ExceptionListener.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // Get the exception object from the received event
        $exception = $event->getException();
        // Customize response object to display the exception details

        if ($exception instanceof ApiException) {
        	$response = array(
        		"status" => "KO",
        		"errorCode" => $exception->getErrorCode(),
        		"error_description" => $exception->getMessage()
        	);
			$response = new JsonResponse($response);
        	$event->setResponse($response);
        }
    }
}