<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginControllerTest extends WebTestCase
{
	public function testCompleteScenario()
	{
		// Create a new client to browse the application
		$client = static::createClient();
	
		$server = new ApiAuthenticator
		
		// Create a new entry in the database
		$crawler = $client->request('GET', '/api/login');
		$this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /api/login/");
	
		// Fill in the form and submit it
		$form = $crawler->selectButton('Login')->form(array(
				'form[grant_type]'  => 'password',
				'form[email]'  => 'user@user.com',
				'form[password]'  => 'user 123',
		));
	
		$client->submit($form);
		$crawler = $client->followRedirect();

	}
}
