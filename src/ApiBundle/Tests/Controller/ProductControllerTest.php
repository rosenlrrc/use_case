<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
	
	public function testCompleteScenario()
	{
		// Create a new client to browse the application
		$client = static::createClient();
	
		// Create a new entry in the database
		$crawler = $client->request('GET', '/api/product-list');
		$this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /api/product-list/");
		$crawler = $client->click($crawler->filter('a[title="create"]')->link());
	
		// Fill in the form and submit it
		$form = $crawler->selectButton('Create')->form(array(
				'product[name]'  => 'Test',
				'product[price]'  => 10,
				'product[available]'  => true,
				'product[description]'  => 'Test description',
				// ... other fields to fill
		));
	
		$client->submit($form);
		$crawler = $client->followRedirect();

		// Check data in the show view
		$this->assertGreaterThan(0, $crawler->filter('td:contains("Test")')->count(), 'Missing element td:contains("Test")');

		// Edit the entity
		$crawler = $client->click($crawler->selectLink('Update')->link());
	
		$form = $crawler->selectButton('Update')->form(array(
				'product[name]'  => 'Foo',
				'product[price]'  => 9,
				'product[available]'  => false,
				'product[description]'  => 'Foo description',
				// ... other fields to fill
		));
	
		$client->submit($form);
		$crawler = $client->followRedirect();
	
		// Check the element contains an attribute with value equals "Foo"
		$this->assertGreaterThan(0, $crawler->filter('[value="Foo"]')->count(), 'Missing element [value="Foo"]');
	
		// Delete the entity
		$client->submit($crawler->selectButton('Delete')->form());
		$crawler = $client->followRedirect();
	
		// Check the entity has been delete on the list
		$this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
	}
}
