<?php

namespace ApiBundle\Exception;

/**
 * ResourceNotFoundException.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ResourceNotFoundException extends ApiException
{

    public function __construct($message = "Resource not found.")
    {
    	$errorCode = 'ERR002';

    	parent::__construct($errorCode, $message, ApiException::HTTP_STATUS_CODE['NOT_FOUND']);
    }

}
