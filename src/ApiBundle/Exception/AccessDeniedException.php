<?php

namespace ApiBundle\Exception;

/**
 * AccessDeniedException.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class AccessDeniedException extends ApiException
{
	
    public function __construct($message = "You don't have permissions to access this request.")
    {
    	$errorCode = 'ERR003';
    	
    	parent::__construct($errorCode, $message, ApiException::HTTP_STATUS_CODE['FORBIDDEN']);
    }

}
