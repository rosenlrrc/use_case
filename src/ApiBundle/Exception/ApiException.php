<?php

namespace ApiBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * ApiException.
 *
 * @author Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ApiException extends HttpException
{

	const HTTP_STATUS_CODE = array(
		'UNAUTHORIZED' => 401,
		'FORBIDDEN' => 403,
		'NOT_FOUND' => 404,
		'INTERNAL' => 500
	);
	
	private $errorCode;
	
    public function __construct($errorCode, $errorMessage, $httpStatus = self::HTTP_STATUS_CODE['INTERNAL'])
    {
    	$this->errorCode = $errorCode;

    	//Do not pass errorCode because expects integer and string given
        parent::__construct($httpStatus, $errorMessage);
    }

    public function getErrorCode(){
    	return $this->errorCode;
    }
    
}
