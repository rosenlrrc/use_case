<?php

namespace ApiBundle\Exception;

/**
 * TokenNotFoundException.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class TokenNotFoundException extends ApiException
{

    public function __construct($message = "Could not authenticate the user with the given token")
    {
    	$errorCode = 'ERR001';

    	parent::__construct($errorCode, $message, ApiException::HTTP_STATUS_CODE['UNAUTHORIZED']);
    }

}
