<?php

namespace ApiBundle\Entity;

use FOS\OAuthServerBundle\Entity\AccessToken as BaseAccessToken;
use Doctrine\ORM\Mapping as ORM;

/**
 * AccessToken
 */
class AccessToken extends BaseAccessToken
{
	/**
	 * @var int
	 */
	protected $id;
	
	/**
	 * @var int
	 */
	protected $client;
	
	/**
	 * @var int
	 */
	protected $user;
}
