<?php

namespace AppBundle\Factory;

use AppBundle\Entity\Product;

use Doctrine\ORM\EntityManager;

use ApiBundle\Exception\ResourceNotFoundException;

/**
 * ProductFactory.
 *
 * @author  Rosendo Leonardo Hernández Claro <rosendo.hernandez@inmediastudio.com>
 */
class ProductFactory
{
	protected $em;
	
	public function __construct(EntityManager $em){
		$this->em = $em;
	}

	public function getProductDetail($id){
		$product = $this->em->getRepository('AppBundle:Product')->find($id);

		if(!$product instanceof Product){
			throw new ResourceNotFoundException();
		}
		
		return $this->generateProductResponse($product, true);
	}
	
	public function getProductsList(){
		$response = array();
		$products = $this->em->getRepository('AppBundle:Product')->findAll();

		foreach($products as $index => $product){
			$response[$index] = $this->generateProductResponse($product);
		}
		return $response;
	}
	
	public function generateProductResponse($product, $productDetail = false){
			
		$response = array(
			'id' => $product->getId(),
			'name' => $product->getName(),
			'price' => $product->getPrice(),
			'available' => $product->getAvailable()
		);
		
		if($productDetail){
			$response['description'] = $product->getDescription();
			$response['createdAt'] = $product->getCreatedAt()->format('Y-m-d H:i:s');
		}
		
		return $response;
	}

}
