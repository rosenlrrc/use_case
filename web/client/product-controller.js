'use strict';
 
var app = angular.module('myApp.product', ['ngRoute']);

    app.config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/product-list', {
        templateUrl: 'product_list.html',
        controller: 'ProductController'
      });

      $routeProvider.when('/product-create', {
        templateUrl: 'product_create.html',
        controller: 'ProductController'
      });

      $routeProvider.when('/product-show/:id', {
        templateUrl: 'product_show.html',
        controller: 'ProductController',
	params: {
		id: null,
	}
      });

      $routeProvider.when('/product-update/:id', {
        templateUrl: 'product_create.html',
        controller: 'ProductController',
	params: {
		id: null,
	}
      });

    }])
 
    .controller('ProductController', function($scope,$routeParams,$http,$location,URL) {

var CONSTANT_PRODUCT_URL = URL + 'product';
                
      var show = function(id){

                $http({
          method: 'GET',
          url: CONSTANT_PRODUCT_URL + '/' + id,
          headers: {'Authorization': 'Bearer ' + sessionStorage.getItem('token')},
          crossDomain: true,
        
      }).success(function(response){
console.log('show', response);
          $scope.product = response.data;
    }).error(function(data, status, headers, config){
                              console.log('err', data, status, headers, config);
                                  alert(data.error_description);
                                  });
 
      }
 
	var get = function(){
 console.log('headers', sessionStorage.getItem('token'));
        $http({
                    method: 'GET',
          url: CONSTANT_PRODUCT_URL,
          headers: {'Authorization': 'Bearer ' + sessionStorage.getItem('token')},
          crossDomain: true,
          
        }).success(function(response){
console.log('data', response);
          $scope.products = response.data;
        });
if($routeParams.id){
	show($routeParams.id);
}
 
      }

	var getFormAttributes = function(){
console.log('getFormAttributes called');
		return {
		name: $scope.product.name,
		price: $scope.product.price,
		available: $scope.product.available,
		description: $scope.product.description
	};

	}
                
                var parseResponse = function(response){
                if(typeof response !== 'object'){
                response = JSON.parse(response);
                }
                return response;
                }

	get();

      $scope.create = function(){

	var attributes = getFormAttributes();
console.log('attrs', attributes);
                
        $http({
          method: 'POST',
          url: CONSTANT_PRODUCT_URL,
          data: attributes,
          headers: {'Authorization': 'Bearer ' + sessionStorage.getItem('token')},
	crossDomain: true,
        }).success(function(data){
		$location.path( "/product-list" );
                   }).error(function(data, status, headers, config){
                            console.log('err', data, status, headers, config);
                            data = parseResponse(data);
                            var _error = data.error_description !== undefined ? data.error_description : 'An error ocurred.';
                            alert(_error);
                            });
      };

      $scope.update = function(){

	var id = $routeParams.id;
	var attributes = getFormAttributes();
console.log('attrs', attributes);
                
        $http({method: 'PUT',
url: CONSTANT_PRODUCT_URL + '/' + id,
          data: attributes,
          headers: {'Authorization': 'Bearer ' + sessionStorage.getItem('token')},
	crossDomain: true,
}).success(function(data){
		$location.path( "/product-list" );
           }).error(function(data, status, headers, config){
                    console.log('err', data, status, headers, config);
                    data = parseResponse(data);
                    var _error = data.error_description !== undefined ? data.error_description : 'An error ocurred.';
                    alert(_error);
                    });
 
      }
      
      $scope.delete = function(id){
 
                $http({method: 'DELETE',
url: CONSTANT_PRODUCT_URL + '/' + id,
headers: {'Authorization': 'Bearer ' + sessionStorage.getItem('token')},
          crossDomain: true,
}).success(function(data){
           get();
           }).error(function(data, status, headers, config){
                    console.log('err', data, status, headers, config);
                    data = parseResponse(data);
                    var _error = data.error_description !== undefined ? data.error_description : 'An error ocurred.';
                    alert(_error);
                    });
 
      }

    }
);
