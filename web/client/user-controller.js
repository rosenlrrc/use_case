'use strict';
 
angular.module('myApp.user', ['ngRoute'])
 
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'login.html',
            controller: 'UserController'
        });
    }])
 
    .controller('UserController', function($scope,$http,$location,URL) {
                console.log('url', URL + 'login');
      $scope.login = function(){

$http({
  method: 'POST',
  url: URL + 'login',
  data: {grant_type: 'password', email: $scope.formData.email, password: $scope.formData.password},
	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	crossDomain: true,
    transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    },
})
        .success(function(response){
console.log('response', response);
	sessionStorage.setItem('token', response.access_token);
            $location.path( "/product-list" );
        })
        .error(function(data, status, headers, config){
console.log('err', data, status, headers, config);
               alert(data.error_description);
        })
        ;
      }

        });